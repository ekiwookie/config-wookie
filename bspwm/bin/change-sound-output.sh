#!/bin/bash

actual_sink=$(pactl get-default-sink)
sinks=$(pactl list short sinks)
line_number=$(echo "$sinks" | grep -n "$actual_sink" | cut -d':' -f1)
let line_number=line_number+1

if [ "$line_number" -gt "$(echo "$sinks" | wc -l)" ]; then
    next_sink_line=$(echo "$sinks" | sed -n '1p')
else
    next_sink_line=$(echo "$sinks" | sed -n "${line_number}p")
fi
next_sink=$(echo "$next_sink_line" | awk '{print $2}')

pactl set-default-sink "$next_sink"


icon=/usr/share/icons/Adwaita/symbolic/status/audio-volume-medium-symbolic.svg
notify-send "Change output device to $next_sink" -i $icon
