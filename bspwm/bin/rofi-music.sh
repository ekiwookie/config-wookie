#!/usr/bin/env bash

radios=(\
"AdroitJazzUnderground@https://icecast.walmradio.com:8443/jazz"
"KingDub@http://london-dedicated.myautodj.com:8862/stream" \
"DubShack@http://streamer.radio.co/s0635c8b0d/listen?fbclid=IwAR16t5mC5UFT9Fp8pbWe0dvYn9VEI3FYJTogE5AJtUF3G1dJi38lwLgD9as"
"Mississipi@https://radiosuitenetwork.torontocast.stream/amississippiblues/" \
"JazzRadioBlues@http://jazzblues.ice.infomaniak.ch/jazzblues-high.mp3" \
"181FMBlues@http://www.181.fm/stream/pls/181-blues.pls" \
"Groovy@http://kotdt.com:8000/groovetime" \
"SOMA_n5MD@https://ice2.somafm.com/n5md-128-mp3" \
"Rain@https://maggie.torontocast.com:2020/stream/natureradiorain" \
)
play () {
    TRACK=$1

    mpc clear > /dev/null  2>&1
    mpc add "$TRACK" > /dev/null  2>&1
    mpc play > /dev/null  2>&1
}
handle_query () {
    QUERY=$1

    if [ "$QUERY" = 'Toggle' ]; then
        mpc toggle > /dev/null  2>&1
        return
    fi
    for item in "${radios[@]}"
    do
        if [ "${item%@*}" = "$QUERY" ]; then
            play "${item#*@}"
            return
        fi
    done
    play "$@"
}
if [ "$@" ]; then
    handle_query "$@"
else
    echo Toggle
    for item in "${radios[@]}"
    do
        echo "${item%@*}"
    done
fi

