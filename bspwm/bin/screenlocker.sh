#! /bin/bash


pgrep -x xidlehook > /dev/null || xidlehook \
      --not-when-fullscreen \
      --timer 300 \
        'xrandr --output eDP-1 --brightness .5' \
        'xrandr --output eDP-1 --brightness 1' \
      --timer 15 \
        'xrandr --output eDP-1 --brightness .3; setxkbmap "us"; betterlockscreen -l' \
        'xrandr --output eDP-1 --brightness 1'\
      --timer 300 \
        'xrandr --output eDP-1 --brightness 0' \
        'xrandr --output eDP-1 --brightness 1'\
      --timer 3600 \
        'systemctl suspend' \
        '' &
