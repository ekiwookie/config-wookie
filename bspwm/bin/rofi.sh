#! /bin/bash

function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    LIST_WHITESPACES=`echo $LIST | tr "$DELIMITER" " "`
    for x in $LIST_WHITESPACES; do
        if [ "$x" = "$VALUE" ]; then
            return 0
        fi
    done
    return 1
}
if exists_in_list "window drun calc" " " $1; then
    PARAMS='-show '$1
else
    PARAMS='-show '$1' -modi '$1':'$HOME'/.config/bspwm/bin/rofi-'$1'.sh'
fi


CURRENT_LAYOUT=$(setxkbmap -query | awk -F : 'NR==3{print $2}' | sed 's/ //g')

setxkbmap "us"
rofi $PARAMS
setxkbmap "$CURRENT_LAYOUT"
