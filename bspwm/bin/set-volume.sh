#! /bin/bash

get_volume() {
    volume=$(pactl get-sink-volume 0 | grep '^Volume:' | \
        head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
    echo $volume
}

set_volume() {
    if [ "$1" = 'toggle' ]; then
        pactl set-sink-mute 0 toggle
        volume=$(get_volume)
    else
        if [ "$1" = 'up' ]; then
            pactl set-sink-volume 0 +5%
        elif [ "$1" = 'down' ]; then
            pactl set-sink-volume 0 -5%
        else
            return
        fi
        volume=$(get_volume)
    fi

    ismute=$(pactl get-sink-mute 0 | grep yes)

    if [[ -n $ismute ]] ; then
        icon=/usr/share/icons/Adwaita/symbolic/status/audio-volume-muted-symbolic.svg
    elif [ "$volume" -lt 33 ]; then
        icon=/usr/share/icons/Adwaita/symbolic/status/audio-volume-low-symbolic.svg
    elif [ "$volume" -gt 66 ]; then
        icon=/usr/share/icons/Adwaita/symbolic/status/audio-volume-high-symbolic.svg
    else
        icon=/usr/share/icons/Adwaita/symbolic/status/audio-volume-medium-symbolic.svg
    fi

    notify-send "Volume" -h int:value:$volume -i $icon
}

set_volume $1
