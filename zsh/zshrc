HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=1000
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/ekiwookie/.zshrc'


export EDITOR=nvim
export VISUAL=nvim
export TERM=xterm-256color
# export TERM=screen-256color

export PATH=/home/ekiwookie/.cargo/bin:/home/ekiwookie/bin:$PATH
# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.


# vi mode
bindkey -v
export KEYTIMEOUT=1
bindkey "^P" history-search-backward
bindkey "^N" history-search-forward


# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
# bindkey -v '^?' backward-delete-char


# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh


bindkey '^ ' autosuggest-accept


if [ -x "$(command -v ranger)" ]; then
    alias r=". ranger ranger"
    alias R="ranger"
fi

if [ -x "$(command -v yazi)" ]; then
    # alias y="yazi"
fi

function y() {
    local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
    yazi "$@" --cwd-file="$tmp"
    if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        builtin cd -- "$cwd"
    fi
    rm -f -- "$tmp"
}


if [ -x "$(command -v lazygit)" ]; then
    alias l="lazygit"
fi

if [ -x "$(command -v nvim)" ]; then
    alias n="nvim"
fi

if [ -x "$(command -v exa)" ]; then
    alias ls="exa --group-directories-first"
else
    alias ls='ls --color=auto --group-directories-first'
fi
alias ll='ls -alh'
alias la='ls -a'

if [ -x "$(command -v bat)" ]; then
    alias cat="bat"
fi

# if [ -x "$(command -v fzf)" ]; then
#     source <(fzf --zsh)
# fi

if [ -z "$TMUX" ]; then
    tmux attach -t TMUX || tmux new -s TMUX
fi

export PYENV_ROOT="$HOME/.pyenv"
if [ -d "$PYENV_ROOT" ]; then
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

[ -s "/usr/share/nvm/init-nvm.sh" ] &&  source /usr/share/nvm/init-nvm.sh
