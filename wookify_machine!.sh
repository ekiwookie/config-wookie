#!/bin/bash


config_dir="$PWD"
add_tmux=true
add_nvim=true
add_zsh=true

is_add () {
  if [ $1 = true ];
    then echo $2;
    else echo "";
  fi
}
# files=" \
#   $(is_add $add_tmux $HOME/.tmux.conf) \
#   $(is_add $add_nvim $HOME/.config/nvim) \
#   $(is_add $add_nvim $HOME/.cache/nvim) \
#   $(is_add $add_nvim $HOME/.local/share/nvim) \
#   $(is_add $add_nvim $HOME/.local/state/nvim) \
# "
# tar zcvf $PWD/backups/`date +%Y-%m-%dT%T`.tar.gz $files > /dev/null

ln -srf bspwm $HOME/.config/bspwm;
ln -srf polybar $HOME/.config/polybar;
ln -srf dunst $HOME/.config/dunst;
ln -srf rofi $HOME/.config/rofi;
ln -srf alacritty $HOME/.config/alacritty;
ln -srf picom $HOME/.config/picom;

if [ $add_tmux = true ]; then
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  ln -srf tmux.conf $HOME/.tmux.conf;
fi

if [ $add_nvim = true ]; then
  rm -rf $HOME/.cache/nvim
  rm -rf $HOME/.local/share/nvim
  rm -rf $HOME/.local/state/nvim
  rm -rf $HOME/.config/nvim
  ln -srf nvim $HOME/.config/nvim;
  ln -srf .vimrc $HOME/.vimrc;
fi

if [ $add_zsh = true ]; then
  git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.zsh/zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k
  ln -srf zsh/zshrc $HOME/.zshrc;
  ln -srf zsh/.p10k.zsh $HOME/.p10k.zsh;
fi

echo "This machine is WOOKIFIED!"
