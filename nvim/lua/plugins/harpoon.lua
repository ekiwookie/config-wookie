return {
	"ThePrimeagen/harpoon",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	branch = "harpoon2",
	lazy = false,
	config = function()
		local keymap = vim.keymap -- for conciseness
		local harpoon = require("harpoon")

		keymap.set("n", "<leader>hh", function()
			harpoon:list():add()
		end, { desc = "Mark file with harpoon", silent = true })
		keymap.set("n", "]h", function()
			harpoon:list():next()
		end, { desc = "Go to next harpoon mark", silent = true })
		keymap.set("n", "[h", function()
			harpoon:list():prev()
		end, { desc = "Go to previous harpoon mark", silent = true })
		keymap.set("n", "<leader>hl", function()
			harpoon.ui:toggle_quick_menu(harpoon:list())
		end, { desc = "Harpoon list", silent = true })
		keymap.set("n", "<leader>h1", function()
			harpoon:list():select(1)
		end, { desc = "Harpoon 1", silent = true })
		keymap.set("n", "<leader>h2", function()
			harpoon:list():select(2)
		end, { desc = "Harpoon 2", silent = true })
		keymap.set("n", "<leader>h3", function()
			harpoon:list():select(3)
		end, { desc = "Harpoon 3", silent = true })
		keymap.set("n", "<leader>h4", function()
			harpoon:list():select(4)
		end, { desc = "Harpoon 4", silent = true })
	end,
}
