return {
    "codethread/qmk.nvim",
    ft = "devicetree",
    opts = {
        name = "zmk",
        -- layout = { { "x", "x" } },
        layout = {
            "x x x x x x x x x x x x",
            "x x x x x x x x x x x x",
            "x x x x x x x x x x x x",
            "_ _ _ x x x x x x _ _ _",
        },

        variant = "zmk",
    },
}
