-- local lga_actions = require("telescope-live-grep-args.actions")
local actions = require("telescope.actions")

return {
	"nvim-telescope/telescope-ui-select.nvim",
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				build = "make",
			},
		},
		opts = function()
			return {
				extensions = {
					["ui-select"] = {
						require("telescope.themes").get_dropdown({}),
					},
				},
				defaults = {
					path_display = { "truncate " },
					-- path_display = { "shorten" },
					mappings = {
						i = {
							["<M-p>"] = require("telescope.actions.layout").toggle_preview,
							["<C-h>"] = require("telescope.actions").preview_scrolling_left,
							["<C-l>"] = require("telescope.actions").preview_scrolling_right,
							["<C-j>"] = require("telescope.actions").preview_scrolling_down,
							["<C-k>"] = require("telescope.actions").preview_scrolling_up,
							["<M-l>"] = require("telescope.actions").results_scrolling_right,
							["<M-h>"] = require("telescope.actions").results_scrolling_left,

							-- ["<C-k>"] = lga_actions.quote_prompt(),
							-- ["<C-i>"] = lga_actions.quote_prompt({ postfix = " --iglob " }),
							-- freeze the current list and start a fuzzy search in the frozen list
							["<C-space>"] = actions.to_fuzzy_refine,
						},
					},
					sorting_strategy = "ascending",
					layout_config = {
						horizontal = {
							prompt_position = "top",
						},
					},
					preview = {
						hide_on_startup = false,
						timeout = 50,
					},
					extensions = {
						fzf = {
							fuzzy = true, -- false will only do exact matching
							override_generic_sorter = true, -- override the generic sorter
							override_file_sorter = true, -- override the file sorter
							case_mode = "smart_case", -- or "ignore_case" or "respect_case"
							-- the default case_mode is "smart_case"
						},
					},
				},
			}
		end,
		init = function()
			require("telescope").load_extension("ui-select")
			require("telescope").load_extension("noice")
			local builtin = require("telescope.builtin")

			vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "Find files" })
			vim.keymap.set("n", "<leader>fF", function()
				builtin.find_files({ hidden = true, no_ignore = true })
			end, { desc = "Find all files" })
			vim.keymap.set("n", "<leader>fw", builtin.live_grep, { desc = "Find word in files" })
			vim.keymap.set("n", "<leader>fb", builtin.buffers, { desc = "Find buffers" })
			vim.keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "Help Tags" })
			vim.keymap.set("n", "<leader>ft", ":TodoTelescope<CR>", { desc = "Todo" })
			vim.keymap.set(
				"n",
				"<leader>fc",
				"<cmd>Telescope grep_string<cr>",
				{ desc = "Find string under cursor in cwd" }
			)
			vim.keymap.set("n", "<leader>gb", builtin.git_branches, { desc = "Git branches" })
			vim.keymap.set("n", "<leader>gc", builtin.git_commits, { desc = "Git Commits" })
			vim.keymap.set("n", "<leader>gs", builtin.git_status, { desc = "Git status" })
			vim.keymap.set("n", "<leader>ls", builtin.lsp_document_symbols, { desc = "LSP symbols" })
			vim.keymap.set("n", "gr", builtin.lsp_references, { noremap = true, silent = true, desc = "References" })
			vim.keymap.set("n", "gd", builtin.lsp_definitions, { noremap = true, silent = true, desc = "Definitions" })
			vim.keymap.set("n", "<leader>f<cr>", builtin.resume, { desc = "Resume last search" })

			vim.keymap.set("n", "<leader>fj", builtin.jumplist, { desc = "Jumplist" })
			vim.keymap.set("n", "<leader>fn", ":Telescope notify<CR>", { desc = "Notifications" })
		end,
	},
}
