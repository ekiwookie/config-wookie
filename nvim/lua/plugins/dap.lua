return {
    "mfussenegger/nvim-dap",
    dependencies = {
        "rcarriga/nvim-dap-ui",
    },
    config = function()
        vim.keymap.set("n", "<leader>dc", function()
            require("dap").continue()
        end, { desc = "Continue" })
        vim.keymap.set("n", "<leader>dl", function()
            require("dap").run_last()
        end, { desc = "Run last" })
        vim.keymap.set("n", "<leader>db", function()
            require("dap").toggle_breakpoint()
        end, { desc = "Breakpoint" })
        vim.keymap.set("n", "<leader>dr", function()
            require("dap").run_to_cursor()
        end, { desc = "Run to cursor" })
        vim.keymap.set("n", "<leader>dB", function()
            require("dap").set_breakpoint(vim.fn.input("Breakpoint condition: "))
        end, { desc = "Conditional breakpoint" })

        vim.keymap.set("n", "<leader>dC", function()
            require("dap").terminate()
        end, { desc = "Cancel" })

        vim.keymap.set("n", "<F5>", require("dap").continue)
        vim.keymap.set("n", "<F10>", require("dap").step_over)
        vim.keymap.set("n", "<F7>", require("dap").step_into)
        vim.keymap.set("n", "<F8>", require("dap").step_out)
        -- require("dap.ext.vscode").load_launchjs(nil, {
        --     ["python"] = {
        --         "python",
        --     },
        --     ["pwa-node"] = {
        --         "javascript",
        --         "typescript",
        --     },
        --     ["node"] = {
        --         "javascript",
        --         "typescript",
        --     },
        --     ["cppdbg"] = {
        --         "c",
        --         "cpp",
        --     },
        --     ["dlv"] = {
        --         "go",
        --     },
        -- })
    end,
}
