return {
	"epwalsh/obsidian.nvim",
	version = "*",
	lazy = false,
	ft = "markdown",
	event = {
		"BufReadPre /home/ekiwookie/Sync/ekiwiki",
		"BufNewFile /home/ekiwookie/Sync/ekiwiki",
	},
	dependencies = {
		"nvim-lua/plenary.nvim",

		-- see below for full list of optional dependencies 👇
	},
	opts = {
		workspaces = {
			{
				name = "personal",
				path = "/home/ekiwookie/Sync/ekiwiki",
			},
			-- {
			-- 	name = "work",
			-- 	path = "~/vaults/work",
			-- },
		},
		ui = {
			enable = true,
		},

		-- see below for full list of options 👇
	},
	keys = {
		{
			"<leader>fo",
			":ObsidianSearch<cr>",
			silent = true,
			desc = "Dismiss All Notifications",
		},
		{
			"<leader>ob",
			":ObsidianBacklinks<cr>",
			silent = true,
			desc = "Obsidian backlinks",
		},
	},
}
