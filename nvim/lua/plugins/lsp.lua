return {
	"neovim/nvim-lspconfig",
	event = {
		"BufReadPre",
		"BufNewFile",
	},
	dependencies = {
		{
			"williamboman/mason.nvim",
			cmd = "Mason",
			config = true,
		},
		"williamboman/mason-lspconfig.nvim",
		{
			"j-hui/fidget.nvim",
			tag = "legacy",
			opts = {},
		},
		"folke/neodev.nvim",
	},
	config = function(_, _)
		local mason_lspconfig = require("mason-lspconfig")
		local lspconfig = require("lspconfig")
		local lsp = require("plugins.lsp")

		local servers = {
			ruff_lsp = {},
			jedi_language_server = {},
			lua_ls = {
				Lua = {
					workspace = { checkThirdParty = false },
					telemetry = { enable = false },
				},
			},
			tsserver = {},
			bashls = {},
			-- TODO: find true name for debug py
			-- debugpy = {},
		}
		require("lspconfig").gdscript.setup({
			cmd = { "nc", "127.0.0.1", "6005" },
		})
		require("neodev").setup()

		mason_lspconfig.setup({
			ensure_installed = vim.tbl_keys(servers),
			automatic_installation = true,
		})

		-- local capabilities = vim.lsp.protocol.make_client_capabilities()
		local capabilities = require("cmp_nvim_lsp").default_capabilities()
		capabilities.hover = false

		mason_lspconfig.setup_handlers({
			function(server_name)
				lspconfig[server_name].setup({
					capabilities = capabilities,
					on_attach = lsp.on_attach,
					settings = servers[server_name],
					filetypes = (servers[server_name] or {}).filetypes,
				})
			end,
		})
	end,
	on_attach = function(_, bufnr)
		local nmap = function(keys, func, desc)
			if desc then
				desc = "LSP: " .. desc
			end

			vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
		end

		vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
		vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
		vim.keymap.set("n", "<leader>lD", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
		vim.keymap.set("n", "<leader>ld", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })

		nmap("<leader>la", vim.lsp.buf.code_action, "Code action")
		nmap("<leader>lr", vim.lsp.buf.rename, "Rename")

		nmap("<leader>jd", vim.lsp.buf.definition, "[J]ump to [D]efinition")
		nmap("<leader>jr", require("telescope.builtin").lsp_references, "[J]ump to [R]eferences")
		nmap("<leader>jI", require("telescope.builtin").lsp_implementations, "[J]ump to [I]mplementation")
		nmap("<leader>jt", vim.lsp.buf.type_definition, "[T]ype Definition")
		nmap("<leader>jD", vim.lsp.buf.declaration, "[J]ump to [D]eclaration")

		nmap("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")

		nmap("<leader>lh", vim.lsp.buf.hover, "Hover Documentation")
		nmap("<leader>lH", vim.lsp.buf.signature_help, "Signature Documentation")

		nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
		nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
		nmap("<leader>wl", function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, "[W]orkspace [L]ist Folders")
	end,
}
