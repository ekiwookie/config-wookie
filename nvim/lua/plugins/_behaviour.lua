return {
	{
		"gpanders/editorconfig.nvim",
		event = { "BufReadPost", "BufNewFile" },
	},
	{
		-- "phaazon/hop.nvim",
		"smoka7/hop.nvim",
		event = { "BufReadPost", "BufNewFile" },
		config = function(_, opts)
			local hop = require("hop")
			local directions = require("hop.hint").HintDirection
			hop.setup(opts)
			vim.keymap.set("", "f", function()
				hop.hint_words({})
			end, { remap = true })
		end,
	},
	{
		"kevinhwang91/nvim-ufo",
		dependencies = {
			"kevinhwang91/promise-async",
		},
		event = { "BufReadPost", "BufNewFile" },
		opts = {

			provider_selector = function(bufnr, filetype, buftype)
				return { "lsp", "indent" }
			end,
		},
	},
	{
		"kylechui/nvim-surround",
		version = "*", -- Use for stability; omit to use `main` branch for the latest features
		event = "InsertEnter",
		config = true,
	},
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		config = true,
	},
	{
		"nmac427/guess-indent.nvim",
		event = { "BufReadPost", "BufNewFile" },
		config = true,
	},
	{
		"ThePrimeagen/vim-be-good",
		cmd = "VimBeGood",
	},
}
