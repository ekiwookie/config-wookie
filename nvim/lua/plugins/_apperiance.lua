return {
	{
		"lukas-reineke/indent-blankline.nvim",
		opts = function()
			local highlight = {
				"gray",
			}

			local hooks = require("ibl.hooks")
			hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
				vim.api.nvim_set_hl(0, "gray", { fg = "#2A2A37" })
			end)

			return {
				indent = {
					char = "▏",
					highlight = highlight,
				},
				scope = {
					enabled = false,
				},
			}
		end,
		main = "ibl",
		event = { "BufReadPost", "BufNewFile" },
	},
	{
		"EdenEast/nightfox.nvim",
		init = function()
			vim.cmd.colorscheme("nightfox")
		end,
	},
	-- {
	-- 	"rebelot/kanagawa.nvim",
	-- 	lazy = false,
	-- 	init = function()
	-- 		vim.cmd.colorscheme("kanagawa-wave")
	-- 	end,
	-- },
	{
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
			"linrongbin16/lsp-progress.nvim",
		},
		opts = {
			options = {
				theme = "everforest",
				globalstatus = true,
			},
		},
	},
	{
		"akinsho/bufferline.nvim",
		event = "VeryLazy",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
		opts = {
			options = {
				show_buffer_close_icons = false,
			},
		},
		keys = {
			{
				"<S-h>",
				":BufferLineCyclePrev<cr>",
				silent = true,
			},
			{
				"<S-l>",
				":BufferLineCycleNext<cr>",
				silent = true,
			},
			{
				"<leader>c",
				":bd<cr>",
				desc = "Close current Buffer",
				silent = true,
			},
			{
				"<leader>C",
				":bd!<cr>",
				desc = "Force close current Buffer",
				silent = true,
			},
			{
				"<leader>bo",
				":BufferLineCloseOthers<cr>",
				desc = "Close other Buffers",
				silent = true,
			},
			{
				"<leader>bb",
				":BufferLinePick<cr>",
				desc = "Pick Buffer",
				silent = true,
			},
		},
	},
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		-- version = "1.6.1",
		version = "2.0.0",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 500
			local wk = require("which-key")
			wk.register({
				["<leader>b"] = { name = "Buffer" },
				["<leader>d"] = { name = "Debug" },
				["<leader>f"] = { name = "Finder" },
				["<leader>g"] = { name = "Git" },
				["<leader>l"] = { name = "LSP" },
				["<leader>n"] = { name = "No" },
				["<leader>w"] = { name = "Workspace" },
				["<leader>r"] = { name = "Replace word with" },
			})
		end,
		config = true,
	},
}
