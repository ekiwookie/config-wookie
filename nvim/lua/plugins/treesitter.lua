return {
	{
		"windwp/nvim-ts-autotag",
		event = { "BufReadPre", "BufNewFile" },
		opts = {},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		event = { "BufReadPre", "BufNewFile" },
		build = ":TSUpdate",
		opts = {
			ensure_installed = {
				"python",
				"typescript",
				"lua",
				"tsx",
				"javascript",
				"regex",
				"bash",
				"markdown",
				"markdown_inline",
				"sql",
				"pug",
			},

			sync_install = false,
			auto_install = true,
			highlight = {
				enable = true,
			},
		},
		config = function(_, opts)
			local treesitter = require("nvim-treesitter.configs")
			treesitter.setup(opts)
		end,
	},
}
