return {
	{
		"rcarriga/nvim-notify",
		event = "VeryLazy",
		opts = {
			stages = "static",
			timeout = 2500,
			render = "minimal",
			max_width = 50,
		},
		keys = {
			{
				"<leader>nn",
				function()
					require("notify").dismiss({ silent = true, pending = true })
				end,
				desc = "Dismiss All Notifications",
			},
		},
	},
}
