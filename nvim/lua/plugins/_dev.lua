return {
	{
		"numToStr/Comment.nvim",
		event = { "BufReadPost", "BufNewFile" },
		config = true,
	},
	{
		"folke/todo-comments.nvim",
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		config = true,
	},
	{
		"lewis6991/gitsigns.nvim",
		event = "BufReadPre",
		config = function(_, opts)
			require("gitsigns").setup(opts)
			vim.keymap.set("n", "<leader>gw", ":Gitsigns blame_line<cr>", { desc = "who is blame?", silent = true })
			vim.keymap.set(
				"n",
				"<leader>gW",
				":Gitsigns toggle_current_line_blame<cr>",
				{ desc = "toggle line blames", silent = true }
			)
			vim.keymap.set("n", "[g", ":Gitsigns prev_hunk<cr>", { desc = "go to prev hunk", silent = true })
			vim.keymap.set("n", "]g", ":Gitsigns next_hunk<cr>", { desc = "go to next hunk", silent = true })
			vim.keymap.set("n", "<leader>gr", ":Gitsigns reset_hunk<cr>", { desc = "reset hunk", silent = true })
			vim.keymap.set("n", "<leader>gd", ":Gitsigns preview_hunk<cr>", { desc = "diff hunk", silent = true })
			vim.keymap.set("n", "<leader>gD", ":Gitsigns diffthis<cr>", { desc = "Diff file", silent = true })
		end,
	},
	{
		"simrat39/symbols-outline.nvim",
		config = true,
		keys = {
			{
				"<leader>ll",
				":SymbolsOutline<CR>",
				silent = true,
			},
		},
	},
	{
		"kdheepak/lazygit.nvim",
		-- event = { "BufReadPost", "BufNewFile" },
		keys = {
			{
				"<leader>gf",
				":LazyGitFilterCurrentFile<CR>",
				silent = true,
			},
			{
				"<leader>gg",
				":LazyGitCurrentFile<CR>",
				silent = true,
			},
		},
		config = function(_, opts)
			vim.g.lazygit_floating_window_scaling_factor = 1.0
		end,
		-- optional for floating window border decoration
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
	},
	{
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-neotest/nvim-nio",
			"nvim-lua/plenary.nvim",
			"antoinemadec/FixCursorHold.nvim",
			"nvim-treesitter/nvim-treesitter",
			"nvim-neotest/neotest-python",
		},
		config = function(_, opts)
			require("neotest").setup({
				adapters = {
					require("neotest-python")({
						runner = "pytest",
					}),
				},
			})
		end,
		keys = {
			{ "<leader>t", "", desc = "+test" },
			{
				"<leader>tt",
				function()
					require("neotest").run.run(vim.fn.expand("%"))
				end,
				desc = "Run File",
			},
			{
				"<leader>tT",
				function()
					require("neotest").run.run(vim.uv.cwd())
				end,
				desc = "Run All Test Files",
			},
			{
				"<leader>tr",
				function()
					require("neotest").run.run()
				end,
				desc = "Run Nearest",
			},
			{
				"<leader>tl",
				function()
					require("neotest").run.run_last()
				end,
				desc = "Run Last",
			},
			{
				"<leader>ts",
				function()
					require("neotest").summary.toggle()
				end,
				desc = "Toggle Summary",
			},
			{
				"<leader>to",
				function()
					require("neotest").output.open({ enter = true, auto_close = true })
				end,
				desc = "Show Output",
			},
			{
				"<leader>tO",
				function()
					require("neotest").output_panel.toggle()
				end,
				desc = "Toggle Output Panel",
			},
			{
				"<leader>tS",
				function()
					require("neotest").run.stop()
				end,
				desc = "Stop",
			},
			{
				"<leader>tw",
				function()
					require("neotest").watch.toggle(vim.fn.expand("%"))
				end,
				desc = "Toggle Watch",
			},
		},
	},
}
