return {
    is_ssh = function()
        return vim.env.SSH_CONNECTION ~= nil
    end,
}
