return {
    {
        "mxsdev/nvim-dap-vscode-js",
        enabled = not require("functions").is_ssh(),
        dependencies = {
            "mfussenegger/nvim-dap",
            {
                "microsoft/vscode-js-debug",
                version = "1.x",
                build = "npm i && npm run compile vsDebugServerBundle && mv dist out",
            },
        },
        ft = {
            "js",
            "vue",
        },
        opts = function()
            return {
                debugger_path = vim.fn.stdpath("data") .. "/lazy/vscode-js-debug",
                adapters = { "pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost" }, -- which adapters to register in nvim-dap
            }
        end,
        config = function(_, opts)
            local dap = require("dap")

            require("dap-vscode-js").setup(opts)
            -- dap.setup(opts)
            for _, language in ipairs({ "typescript", "javascript", "vue", "javascriptreact" }) do
                dap.configurations[language] = {
                    {
                        type = "pwa-chrome",
                        request = "attach",
                        name = "Attach Program (pwa-chrome = { port: 9222 })",
                        program = "${file}",
                        cwd = vim.fn.getcwd(),
                        sourceMaps = true,
                        port = 9222,
                        webRoot = "${workspaceFolder}",
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Current File (pwa-node)",
                        cwd = vim.fn.getcwd(),
                        args = { "${file}" },
                        sourceMaps = true,
                        protocol = "inspector",
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Current File (pwa-node with ts-node)",
                        cwd = vim.fn.getcwd(),
                        runtimeArgs = { "--loader", "ts-node/esm" },
                        runtimeExecutable = "node",
                        args = { "${file}" },
                        sourceMaps = true,
                        protocol = "inspector",
                        skipFiles = { "<node_internals>/**", "node_modules/**" },
                        resolveSourceMapLocations = {
                            "${workspaceFolder}/**",
                            "!**/node_modules/**",
                        },
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Current File (pwa-node with deno)",
                        cwd = vim.fn.getcwd(),
                        runtimeArgs = { "run", "--inspect-brk", "--allow-all", "${file}" },
                        runtimeExecutable = "deno",
                        attachSimplePort = 9229,
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Test Current File (pwa-node with jest)",
                        cwd = vim.fn.getcwd(),
                        runtimeArgs = { "${workspaceFolder}/node_modules/.bin/jest" },
                        runtimeExecutable = "node",
                        args = { "${file}", "--coverage", "false" },
                        rootPath = "${workspaceFolder}",
                        sourceMaps = true,
                        console = "integratedTerminal",
                        internalConsoleOptions = "neverOpen",
                        skipFiles = { "<node_internals>/**", "node_modules/**" },
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Test Current File (pwa-node with vitest)",
                        cwd = vim.fn.getcwd(),
                        program = "${workspaceFolder}/node_modules/vitest/vitest.mjs",
                        args = { "--inspect-brk", "--threads", "false", "run", "${file}" },
                        autoAttachChildProcesses = true,
                        smartStep = true,
                        console = "integratedTerminal",
                        skipFiles = { "<node_internals>/**", "node_modules/**" },
                    },
                    {
                        type = "pwa-node",
                        request = "launch",
                        name = "Launch Test Current File (pwa-node with deno)",
                        cwd = vim.fn.getcwd(),
                        runtimeArgs = { "test", "--inspect-brk", "--allow-all", "${file}" },
                        runtimeExecutable = "deno",
                        attachSimplePort = 9229,
                    },
                    {
                        type = "node2",
                        request = "attach",
                        name = "Attach Program (Node2)",
                        processId = require("dap.utils").pick_process,
                    },
                    {
                        type = "node2",
                        request = "attach",
                        name = "Attach Program (Node2 with ts-node)",
                        cwd = vim.fn.getcwd(),
                        sourceMaps = true,
                        skipFiles = { "<node_internals>/**" },
                        port = 9229,
                    },
                    {
                        type = "pwa-node",
                        request = "attach",
                        name = "Attach Program (pwa-node)",
                        cwd = vim.fn.getcwd(),
                        processId = require("dap.utils").pick_process,
                        skipFiles = { "<node_internals>/**" },
                    },
                }
            end
        end,
    },
}
