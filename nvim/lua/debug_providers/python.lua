return {
    "mfussenegger/nvim-dap-python",
    ft = "python",
    enabled = not require("functions").is_ssh(),
    dependencies = {
        "mfussenegger/nvim-dap",
    },
    config = function(_, opts)
        local path = vim.fn.stdpath("data") .. "/mason/packages/debugpy/venv/bin/python"
        require("dap-python").setup(path)
    end,
}
