vim.g.mapleader = " "

vim.keymap.set("n", "<leader>nh", ":noh<cr>", { desc = "Stop highlighting", silent = true })
vim.keymap.set("n", "<leader>sw", ":set wrap!<cr>", { desc = "Toggle wrap", silent = true })

vim.keymap.set("v", "<S-Tab>", "<gv")
vim.keymap.set("v", "<Tab>", ">gv")

vim.keymap.set("n", "|", ":call append(line('.'), '')<cr>", { silent = true })
vim.keymap.set("n", "\\", ":call append(line('.')-1, '')<cr>k", { silent = true })

vim.keymap.set("v", "<C-k>", ":m '<-2<CR>gv=gv", { silent = true })
vim.keymap.set("v", "<C-j>", ":m '>+1<CR>gv=gv", { silent = true })

-- vim.keymap.set("n", "J", "mzJ`z", { desc = "", silent = "true" })
vim.keymap.set("n", "J", "", { desc = "", silent = true })
vim.keymap.set("n", "<C-d>", "<C-d>zz", { silent = true })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { silent = true })
vim.keymap.set("n", "n", "nzzzv", { desc = "", silent = true })
vim.keymap.set("n", "N", "Nzzzv", { desc = "", silent = true })

-- greatest remap ever
vim.keymap.set("x", "<leader>p", [["_dP]], { silent = true })

-- vim.keymap.set(
-- 	"n",
-- 	"<leader>r",
-- 	[[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
-- 	{ silent = true, desc = "Replace in file" }
-- )
-- vim.keymap.set("v", "<leader>r", ':%s/<C-r><C-r>"//g<Left><Left>', { silent = true, desc = "Replace in file" })
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true, desc = "add executable flag" })

-- SymbolsOutline
-- TODO move to plugin

vim.keymap.set("n", "<leader>ll", ":SymbolsOutline<CR>", { silent = true })
